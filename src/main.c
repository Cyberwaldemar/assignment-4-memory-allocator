
#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <assert.h>
#define BLOCK_MIN_CAPACITY 24
#define INITIAL_SIZE 4096
#define BLOCK_SIZE_1 100
#define BLOCK_SIZE_2 200
#define BLOCK_SIZE_3 50
#define TESTS 100
#define BATCH_SIZE 10

static void test_successful_allocation(){
    
    const void *heap_start = heap_init(INITIAL_SIZE);
    debug_heap(stderr, heap_start);
    
    assert(heap_start != NULL && "Failed to initialize heap");

    void *allocated_memory = _malloc(BLOCK_SIZE_1);
    
    assert(allocated_memory != NULL && "Failed to allocate memory");

    heap_term();
}

static void test_free_single_block(){
    const void *heap = heap_init(1);
    const struct block_header *head = heap;

    assert(size_from_capacity(head->capacity).bytes == REGION_MIN_SIZE && "Building heap internal error");
    assert(head->is_free && "Building heap internal error");
    assert(head->next == NULL && "Building heap internal error");

    void *allocated_memory = _malloc(BLOCK_SIZE_1);
    assert(allocated_memory != NULL && "Failed to allocate memory");

    assert(head->capacity.bytes == BLOCK_MIN_CAPACITY && "Region size invalid");
    assert(!head->is_free);

    _free(allocated_memory);
    assert(head->is_free && "Failed to free");
    assert(head->next == NULL);

    heap_term();
}

static void test_out_of_memory_expansion(){
    const void *heap_start = heap_init(INITIAL_SIZE);

    assert(heap_start != NULL && "Failed to initialize heap");

    void *allocated_memory = _malloc(INITIAL_SIZE + BLOCK_SIZE_1);

    assert(allocated_memory != NULL && "Failed to allocate memory");

    heap_term();
}

static void test_out_of_memory_no_expansion(){
    const void *heap_start = heap_init(INITIAL_SIZE);

    assert(heap_start != NULL && "Failed to initialize heap");

    void *allocated_memory1 = _malloc(BLOCK_SIZE_1);

    assert(allocated_memory1 != NULL && "Failed to allocate memory!");

    void *allocated_memory2 = _malloc(INITIAL_SIZE + BLOCK_SIZE_1);

    assert(allocated_memory2 == NULL && "Memory allocation should fail");
    heap_term();
}
static void test_small_batches(){
    for(size_t i = 0; i < TESTS; ++i){
        fprintf(stderr, "Test %zu", i);
        void *h = heap_init(BLOCK_SIZE_3 * TESTS*32);
        assert(h && "Failed: heap was not initialized properly\n");
        debug_heap(stderr, h);
        void *k;
        for(size_t j = 0; j < TESTS / BLOCK_SIZE_3; ++j){
            k = _malloc(BLOCK_SIZE_3);
        }
        assert(k && "Failed: data was not allocated properly\n");
        debug_heap(stderr, h);
        _free(k);
        debug_heap(stderr, h);
        heap_term();
    }
}


int main(){
    test_successful_allocation();
    test_free_single_block();
    test_out_of_memory_expansion();
    test_out_of_memory_no_expansion();
    test_small_batches();
    return 0;
}
